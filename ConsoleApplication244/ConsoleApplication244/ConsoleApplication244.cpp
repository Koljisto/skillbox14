﻿#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;

#define AUTO 0;

int main()
{

#if AUTO
	std::string str{ "string" };
#else
	std::string str;
	std::getline(cin, str);
#endif

	cout << str << endl;
	cout << str.length() << endl;
	cout << str.front() << endl;
	cout << str.back() << endl;
}